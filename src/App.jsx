import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import MouseContainer from '../src/components/MouseContainer'
import CounterReducerTwo from './components/CounterReducerTwo'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      Hello world !
    </div>
  )
}

export default App
