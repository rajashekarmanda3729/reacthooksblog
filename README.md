#  React-Hooks

* functional component's don't have maintaining state & life-cycle's method's like class-component's.The functional component's also use same feature's what class-component's have that's why introduced React-Hooks in latest version of reactJS.
* Now functional component's also state maintaing & component life-cycle method's using hooks.
* More redable & less syntax compare to class-component's.

# Types of Hooks


## 1 useState

### Usage:
When the UI re-render on that time want a previous data to update current data, In that case we store that type data in state bu using useState hook.It will re-render when the store data will change.


               import React, { useState } from 'react';

                function Example() {
                
                const [count, setCount] = useState(0);

                return (
                    <div>
                    <p>You clicked {count} times</p>
                    <button onClick={() => setCount(count + 1)}>
                        Click me
                    </button>
                    </div>
                )
                }


*  The useState hook lets you add state to functional components.
*  In classes,the state is always an object, But with the useState hook,the state doesn't have to be an object.It can be an [Array,object,number,boolean...etc].
*  The useState hook returns an array with 2 elements.The first element current value of the state, and the second element is the state setter function.


## 2 useEffect

### Usage:
* To fetch Data from API
* To stop timer's
* To Cancel subscription's
* To cancel or remove event litsener's
* Clean up & side effect's

### Example:
        import React, { useState, useEffect } from 'react'

        function MouseContainer() {
            const [x, setX] = useState(0)
            const [y, setY] = useState(0)

            const logMousePosition = e => {
                console.log('mouse event')
                setX(e.clientX)
                setY(e.clientY)
            }

            useEffect(() => {
                window.addEventListener('mousemove', logMousePosition)

                return () => {
                    window.removeEventListener('mousemove', logMousePosition)
                }
            },[])
            return (
                <div>
                    <h1>Hooks - X - {x} Y - {y}</h1>
                </div>
            )
        }

        export default MouseContainer

In class component's life-cycel method's ComponentDidMount,ComponentDidUpdate and ComponentWillUnmount.This method's will call different situation's like 
ComponentDidMount method will call when component "Mounted" (create)
ComponentDidUpdate method will call when component "State change's" (state updated)
ComponentWillUnmount method will call when component "UnMounted" (removed from DOM)

The above all method's we can use in one function simply by using "useEffect" hook.

* Declaring dependecy array-[] we can customize useEffect hook behavior.
* We can return callback function to cleanup activities.

## 3 useContext

### Usage:
* If there is a situation One parent having children component's & Children component having nested children component's. In this situation you want to pass some data to below tree nested-child component.In this case we use "useContext" where we can pass data "Globally" that every component in the tree can access that data by using this hook.

### 3 steps to use useContext
* First we have to create context -using createContext()
* Second we have to wrap Context with Child component's using "<ContextName.Provider>Child component's</ContextName.Provider>
* Third by using "useContext()" we can access that data.

            function Component1() {
            export const [user, setUser] = useState("Jesse Hall");

            return (
                <UserContext.Provider value={user}>
                <h1>{`Hello ${user}!`}</h1>
                <Component2 user={user} />
                </UserContext.Provider>
            );
            }

            functionComponent5(){
                const user = React.useContext(UserContext)
                return (
                    <div>{user}</div>
                )
            }



## 4 useReducer

* When our application grows in size, we will most likely deal with more complex state transitions, at which point we can be better off using useReducer.
* useReducer provides more predictable state transitions than useState, More important when state changes become so complex that you want to have one place to manage state, like the render function

### Usage 
* It's take two parameters one is reducer function & second is initial state value.The reducer function will take tow arguments currentState & action returns a new state.
* By calling dispatch function we can perform action's on state.


### 


            import React from 'react'

            const initialState = {
                firstCount: 0
            }

            const reducer = (state, action) => {
                switch (action.type) {
                    case 'increment':
                        return { firstCount: state.firstCount + 1 }
                    case 'decrement':
                        return { firstCount: state.firstCount - 1 }
                    case 'reset':
                        return initialState
                    default:
                        return state
                }
            }

            function CounterReducerTwo() {
                const [count, dispatch] = React.useReducer(reducer, initialState)
                return (
                    <div>
                        Count {count.firstCount}
                        <button type='button' onClick={() => dispatch({ type: 'increment' })}>Increment</button>
                        <button type='button' onClick={() => dispatch({ type: 'decrement' })}>Decrement</button>
                        <button type='button' onClick={() => dispatch({ type: 'reset' })}>Reset</button>

                    </div>
                )
            }

            export default CounterReducerTwo


## 5 useCallback Hook

### Usage
* useCallback is a hook that will return memoized version of the callback function that only changes if one of the dependencies has changed.
* It is useful when passing callbacks to optimized child components that rely on reference equality to prevent unnecessary renders.
  
* It will take parameter's firstone is callback function and second one is array dependecies.

Example: 
        const incrementAge = useCallback(() => {
            setAge(age + 2)
        },[age])

        const incrementSalary = useCallback(() => {
            setSalary(salary + 2)
        },[salary])

If age & salary won't change the increment function's doesn't excute.By using this hook we can prevent unnecessary execution & Improve performance by preventing it.

## 6 useMemo Hook

### Usage 
* It will take two argument's callback function & dependecies array-[].When dependencies change it will execute. 
* By using this hook we can tell to react not to re-calculate values unnecessary that will improve performance of execution.Especially that long it will take long time to compute.
* useMemo hook only recompute the catched value when one of the dependencies has changed this optimization avoid expensive calculations on every render.

Example: 

            import React, { useState, useMemo } from 'react'

            function Counter() {
                const [counterOne, setCounterOne] = useState(0)
                const [counterTwo, setCounterTwo] = useState(0)

                const incrementOne = () => {
                    setCounterOne(counterOne + 1)
                }

                const incrementTwo = () => {
                    setCounterTwo(counterTwo + 1)
                }

                const isEven = useMemo(() => {
                    let i = 0
                    while (i < 2000000000) i++
                    return counterOne % 2 === 0
                }, [counterOne])

                return (
                    <div>
                        <div>
                            <button onClick={incrementOne}>Count One - {counterOne}</button>
                            <span>{isEven ? 'Even' : 'Odd'}</span>
                        </div>
                        <div>
                            <button onClick={incrementTwo}>Count Two - {counterTwo}</button>
                        </div>
                    </div>
                )
            }

            export default Counter

* In above example it's not necessary to re execute isEven function when Count Tow button clicks it will decrease the performance.In this situation we use useMemo.


## 7 useRef Hook

### Usage
* This hook can makes it possible access DOM nodes directly within functional components.
* It can be used to create generic container's store data to manipulate it can't cause re-render & it will store mutable values.We can use in clear Intervak timer from an event handler.

Example: 

            import React, { useRef, useEffect } from 'react'

            function FocusInput() {

                const inputRef = useRef(null)

                useEffect(() => {
                    inputRef.current.focus()
                }, [])
                
                return (
                    <div>
                        <input ref={inputRef} type="text" />
                    </div>
                )
            }

            export default FocusInput

* In above example we are doing when page-loads that input element will focus by using useRef hook will do.It will give reference of the DOM element.